package eu.awesome.app.controller;

import java.util.LinkedHashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@RequestMapping("/test")
public class AppController {

    @GetMapping(value = "/anonymous", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAnonymous() {
        log.info("getAnonymous called");

        log.info("info log");
        log.trace("trace log");
        log.debug("debug log");
        log.error("error log");
        log.warn("warn log");

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", "Hello world");

        return ResponseEntity.ok(body);
    }


}
